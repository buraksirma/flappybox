#!/usr/bin/env python

import pygame
from pygame.locals import *  # noqa
import sys
import random


class Game:
    playerBoxWidth = 20
    edgeLen = 480
    speed = 5
    wallSpeedByScore = 0 #0.2 if you want to have acceleration in the game
    maxWallSpeed = 6
    gap = 50
    wallStartPosition = -10

    def __init__(self):
        self.screen = pygame.display.set_mode((self.edgeLen, self.edgeLen))
        self.wallWidth = self.edgeLen - self.gap
        self.wallHeight = self.edgeLen / 24
        self.pBoxX = (self.edgeLen - self.playerBoxWidth) / 2
        self.score = 0
        self.playerSurface = pygame.rect.Rect((self.pBoxX, self.edgeLen - self.playerBoxWidth, 
                                            self.playerBoxWidth, self.playerBoxWidth))
        self.leftWallSurface = pygame.rect.Rect((0, 0, self.wallWidth, self.wallHeight))
        self.rightWallSurface = pygame.rect.Rect((0, 0, self.wallWidth, self.wallHeight))
        self.resetWallPositions()
        self.direction = 0
        self.lastPressedKey = None

    def pBoxUpdate(self):
        if self.playerSurface.bottomleft[0] + (self.direction * self.speed) < 0:
            self.playerSurface.bottomleft = self.screen.get_rect().bottomleft
        elif self.playerSurface.bottomright[0] + (self.direction * self.speed) > self.edgeLen:
            self.playerSurface.bottomright = self.screen.get_rect().bottomright
        else:
            self.playerSurface.move_ip(self.direction * self.speed, 0)

    def resetWallPositions(self):
        gapCenter = random.randint(0 + self.gap, self.edgeLen - self.gap)
        self.leftWallSurface.topright = (gapCenter - self.gap / 2, self.wallStartPosition)
        self.rightWallSurface.topleft = (gapCenter + self.gap / 2, self.wallStartPosition)

    def wallsUpdate(self):
        wallSpeed = 3 + self.wallSpeedByScore * self.score
        if wallSpeed > self.maxWallSpeed:
            wallSpeed = self.maxWallSpeed
        self.leftWallSurface.move_ip(0, wallSpeed)
        self.rightWallSurface.move_ip(0, wallSpeed)
        if self.leftWallSurface.top > self.edgeLen:
            self.resetWallPositions()
            self.score += 1

    def run(self):
        clock = pygame.time.Clock()
        pygame.font.init()
        font = pygame.font.SysFont("Arial", 50)
        while True:
            clock.tick(60)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        self.direction = -1
                        self.lastPressedKey = pygame.K_LEFT
                    if event.key == pygame.K_RIGHT:
                        self.direction = 1
                        self.lastPressedKey = pygame.K_RIGHT
                elif event.type == pygame.KEYUP:
                    if event.key == self.lastPressedKey:
                        self.direction = 0
            self.screen.fill((139,195,74))
            pygame.draw.rect(self.screen, (0, 0, 128), self.playerSurface)
            pygame.draw.rect(self.screen, (165, 100, 128), self.leftWallSurface)
            pygame.draw.rect(self.screen, (165, 100, 128), self.rightWallSurface)
            self.screen.blit(font.render(str(self.score), -1, (255, 255, 255)), (200, 50))
            self.pBoxUpdate()
            self.wallsUpdate()
            if(self.playerSurface.colliderect(self.leftWallSurface) or self.playerSurface.colliderect(self.rightWallSurface)):
                self.score = 0
                self.resetWallPositions()
            pygame.display.update()

if __name__ == "__main__":
    Game().run()
